import NewTodo from './components/NewTodo';
import Todos from './components/Todos';
import TodosContextProvider from './store/todos-context';

const Check = (props: any) => {
  return (
    <TodosContextProvider />
  )
}

function App() {
  return (
    <Check>
      <NewTodo />
      <Todos />
    </Check>
  );
}

export default App;